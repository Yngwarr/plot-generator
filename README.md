# Plot Generator

## Installation

1. Clone this repo.
2. `cd` to it's folder.
3. Download dependencies:

```
npm install
# this one is needed to compile the parser
npm install -g nearly
```

4. `make` will compile the parser and run the test.

## Usage

To create PDF-file you will need `pdflatex` installed on your system.

To generate a plot:

1. Write a plot.
2. Run `node main.js plot_file tex_file`, replacing `plot_file` with the name of the file, containing your plot and `tex_file` with the name of the output `*.tex` file.
3. Run `pdflatex tex_file`.
