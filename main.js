const fs = require('fs');
const nearley = require('nearley');
const grammar = require('./grammar.js');
const parser = new nearley.Parser(nearley.Grammar.fromCompiled(grammar));

if (process.argv.length < 4) {
    console.log('Usage: node main.js source_file destination_file');
    return;
}

fs.readFile(process.argv[2], 'utf8', (e, d) => {
	if (e) {
		console.log(err);
		return err;
	}

	parser.feed(d);
	fs.writeFile(process.argv[3], parser.results[0], (err) => {
        if (err) throw err;
        console.log(`The generated LaTeX code is located in '${process.argv[3]}'.`);
    });

});
