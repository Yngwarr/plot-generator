@{%
const moo = require('moo');
const lesc = require('escape-latex');

const lexer = moo.states({
	main: {
		word: {
            match: /[a-zA-Z_]+/,
            keywords: {
                'kw_where': 'where'
            }
        },
        ws: {
            match: /[ \t\n\r]+/,
            lineBreaks: true
        },
		hline: '---',
		at: '@',
		lp: {
			match: '(',
			push: 'mdown'
		},
        rp: ')',
        lsb: '[',
        rsb: ']',
        lcb: {
			match: '{',
			push: 'mdown'
		},
        rcb: '}'
	},
	mdown: {
		tt: {
			match: /[^*_\[\](){}]+/,
			lineBreaks: true
		},
		lp: {
			match: '(',
			push: 'mdown'
		},
        rp: {
			match: ')',
			pop: true
		},
        lsb: '[',
        rsb: ']',
        lcb: {
			match: '{',
			push: 'mdown'
		},
        rcb: {
			match: '}',
			pop: true
		},
		bold: '**',
		italic: '*',
		uline: '_'
	}
});
const nuller = () => { return null; };
const n = (i) => {
    return (d) => {
        return d[i];
    };
};

const header = `
\\documentclass[oneside, a4paper, final, 12pt]{extarticle}
\\usepackage[utf8]{inputenc}
\\usepackage[T2A,T1]{fontenc}
\\usepackage[english]{babel}
\\usepackage{vmargin}
\\usepackage{longtable,tabu}
\\usepackage[nodisplayskipstretch]{setspace}
\\setmarginsrb{2cm}{2cm}{2cm}{2cm}{0pt}{0mm}{0pt}{13mm}
\\sloppy

\\begin{document}
\\onehalfspacing
`;
%}

@lexer lexer

plot ->
	(_ decl {% n(1) %}):* _ {% (d) => {
	return header + '\\begin{longtabu} to \\textwidth{|X[1,l]|X[1,l]|}\\hline'
	+ '\\textbf{VIDEO} & \\textbf{AUDIO} \\\\ \\hline \\endhead '
	+ d[0].join(' ') + '\\hline\\end{longtabu}\\end{document}';
} %}

decl ->
	#at_def {% id %}
	%hline {% (d) => { return ' \n \\hline \n '; } %}
	| block {% id %}

block ->
	left_block _ %lcb _ %rcb {% (d) => { return d[0] + ' & ~ \\\\ '; } %}
	| %lp _ %rp _ right_block {% (d) => { return ' ~ & ' + d[4] + ' \\\\ '; } %}
	| left_block _ right_block {% (d) => {
		return d[0] + ' & '+ d[2] + ' \\\\ ';
	} %}

left_block -> %lp _ text _ %rp {% n(2) %}
right_block -> %lcb _ text _ %rcb {% n(2) %}

text -> chunk:+ {% (d) => { return d[0].join('').concat('\n\n'); } %}
chunk ->
	tt {% (d) => { return lesc(d[0], { preserveFormatting: false }); } %}
	| %bold chunk %bold {% (d) => { return '\\textbf{' + d[1] + '}' } %}
	| %italic chunk %italic {% (d) => { return '\\textit{' + d[1] + '}' } %}
	| %uline chunk %uline {% (d) => { return '\\underline{' + d[1] + '}' } %}

tt -> (%tt|%lp|%rp|%lcb|%rcb|%rsb|%lsb):+ {% (d) => {
	//return d[0].map(v => {return v.value} ).join('');
	return d[0].toString();
} %}

_ -> %ws:? {% nuller %}
__ -> %ws {% nuller %}
