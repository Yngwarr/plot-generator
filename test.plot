(INTRO:) { }

(Clipshow) {
	_OFF_

	This Ludum Dare, I tought I had everything *going* for me. A clean desk,
	a plan, and a resolution -- to lay out coffee for a bit, and I was
	strangely optimistic that the theme would not *suck* for once. There was
	only a minor annoyance: I had to work on Sunday. Surely, this wouldn't pose
	a problem.
}

() {
	_OFF_

	**Or would it?**
}

() { <...> }

---

(TITLE SEQUENCE:) {}

---

(EXTERIOR SHOT OF MEDIA HOUSE AT NIGHT?:){}

(INSERT "2 Days to the jam") {
	_SHOOT_

	(media house at night)
}

() {
	_OFF_

	<...>
}
